from pyspark import SparkConf, SparkContext

from pyspark import SparkFiles
from operator import add
import sys
## Constants
APP_NAME = "Transform"
##OTHER FUNCTIONS/CLASSES

def bad_words(x):
	bad_list=["stupid","bad"]
	for bad_word in bad_list:
		if bad_word in x or bad_word.upper() in x:
			return True
	return False

def main(sc,filename):
     file = sc.textFile(filename)
     good_ones  = file.filter(lambda x : not bad_words(x)   )
     lower_ones = good_ones.map(lambda x : (x.lower(),1) )
     print '\n\n\n\n\ngood ones:'+ str(lower_ones.count()) + '\n\n\n'

     file2 = sc.textFile(filename)
     bad_ones = file.filter(lambda x : bad_words(x))
     print 'bad ones:'+str(bad_ones.count() ) +'\n\n'
 # counts = file.flatMap(lambda line: line.split(" ")) \
           #  .map(lambda word: (word, 1)) \
            # .reduceByKey(lambda a, b: a + b)    
        
    # print lower_ones.count() 
 # upperlines.saveAsTextFile("output")

if __name__ == "__main__":

   # Configure Spark
    conf = SparkConf().setAppName("Spark Count")
    #conf.setMaster('spark://172.22.154.47:7077')
   # conf.set("spark.drive.port",51810)
    sc = SparkContext(conf=conf)
    
  # get threshold
    filename = sys.argv[1]
   # Execute Main functionality
    main(sc, filename)


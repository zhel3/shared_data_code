from pyspark import SparkConf, SparkContext

from pyspark import SparkFiles
from operator import add
import sys
## Constants
APP_NAME = "Transform"
##OTHER FUNCTIONS/CLASSES
def judge(url):
	l = url.split(".")
	if l[0] == 'google':
		return True
	return False

def main(sc,filename):
    file = sc.textFile(filename)
   # lines= file.map(lambda input: input.split(" "))
    google_lines = file.filter(lambda line: judge(line)    )   
# upperlines = lines.map(lambda word:[x for x in word]) 
   # upperlines.saveAsTextFile("output")   
   # output = upperlines.collect() 
   # upperlines.print()	
    print "\n\n\n\n\nresult line numbers:"+str(google_lines.count())+"\n\n\n\n\n"  
   # upperlines.saveAsTextFile("output")

if __name__ == "__main__":

   # Configure Spark
    conf = SparkConf().setAppName("Spark Count")
    #conf.setMaster('spark://172.22.154.47:7077')
   # conf.set("spark.drive.port",51810)
    sc = SparkContext(conf=conf)
    
  # get threshold
    filename = sys.argv[1]
   # Execute Main functionality
    main(sc, filename)

